C探秘68讲贯通C


-----------------------第一章----------xx的分割线-----------------------------
1：获取某个基本数据类型的最大，最小值
	需要头文件 limits 的支持，注意 climits.h是c的头文件。limits 是C++的头文件。
	
	int int_max = std::numeric_limits<int>::max();
	int int_min = std::numeric_limits<int>::min();
	使用 code::blocks会有代码提示功能。
	
	
2: 基本类型和复合类型的区别：
	int i;
	std::string str;
	
	i作为int类型，由于没有及时初始化，导致i为一个随机数。int i(3);//完成初始化
	str是一个类实例，该语句会调用一个无参数的构造方法。str为
	空字符串。 std::string str("abc"); 也可以指定其初始化数据。
	
	
3：cin读取数据。
	string name;
	int age(-1);
	cin>>age>>name;
	//输入 abc123 由于age读取失败，导致后面的读取中断。除非用cin.clear()重置，否则无法继续读取
	
	cin>>name>>age;
	//输入 abc123 由于name是string类型。所以abc123都被name读取。age需要继续输入才能读取。
	
	如果读取失败，那么待输入的变量虽然没有读取到数据，也会影响该变量的值。
	当cin的状态是0的时候
	cin>>age;//age不能被读取，但age的值变成0;
	
	
	
4: 格式化输出。需要头文件 iomanip 的支持。
	std::setw(int n);
	std::setfill(char c);
	注意 setw,setfill 只对后面的一个cout的输出有影响！！
	[code]
	cout<<std::setw(5);
	cout<<std::setfill('0');
	cout<<3<<endl;
	cout<<4<<endl;	//setw,setfill对这个cout没有影响！
	
	设置对其
	cout<<std::right;
	cout<<std::left;

	
5: std::vector 需要头文件vector的支持。它的作用是可变数组。
	使用vector的时候需要指定该元素的类型。
	std::vector<int> nums;	
	std::vector<int> nums(4);	 //初始化有四个元素，每个元素的值为0；
	std::vector<int> nums(6,0);	//初始化有5个元素，每个元素的值为0；
	nums.push_back(3);			//末尾追加元素。用pushback
	nums.back();				//返回最末尾的元素。
	nums.front();				//返回第一个元素
	nums.at(0);					//获取第0个元素。at 比[]运算符号更加安全，当越界后。at会终止程序
	nums.size();				//返回元素的个数。
	
	std::sort(nums.begin(), nums.end());	//排序 需要algorithm 头文件的支持！
	
	iterator的使用：
	
	//vector<int>::iterator it = nums.iterator;  这个是java使用iterator的方法，注意与CPP的区别！！
    vector<int>::iterator it(nums.begin());
    
	while(it!=nums.end())
    {
        cout<<"*it:"<<*it<<endl;
        it++;
    }
	
	用std::copy直接输出vector的内容
	std::copy(nums.begin(), nums.end(), std::ostream_iterator<int>(cout, "\n"));
	前面两个参数表示copy的范围。
	最后一个参数，表示拷贝的目标。
	std::ostream_iterator<int>(cout, "\n");  //need head file: iterator
	
	用std::copy直接读取数据到目标中
	
	std::vector<int> nums;
	std::copy(	std::istream_iterator<int>(std::cin),
				std::istream_iterator<int>(),
				std::back_insert(nums));
	
	第一个参数 std::istream_iterator<int>(std::cin)创建了一个
	读迭代器，以读取输入流，
	第二个参数 std::istream_iterator<int>()表示超出末端的下一个位置的迭代器！
	
6: assert断言，确定断言内部的判断一定为真，否则程序主动奔溃。需要头文件
	#include <cassert>
	assert(3==4);//程序奔溃
	
	
7: 默认bool的输入和输出都是0 或1.
	如果希望输出是ture 和false。
	那么需要
	cout<<std::boolalpha;
	取消
    cout<<std::noboolalpha;
	如果设置了boolalpha，那么读取的时候只能输入true false 否则导致cin变成0.
	
	
8：C++的文件读取。need head file: fstream
	写文件
	std::ifstream wf("./filename");
	wf<<"num:"<<1<<endl;
	关闭文件
	wf.close();
	wf.clear(); 错误状态恢复。
	
	
9：map 数据映射，相当于Java的map(key, value)  头文件 map

	定义map类型变量的时候，和vector一样需要指定数据类型。
	
	map访问某个key的值：
	注意在定义map的之后就指定了map的k 和v 的类型。
	eg: std::map<string, int> m;
	m的key是string， m的v是int。
	m["msg"];//来获取key为msg的值。如果没有该key，则得到的v(int)的值是0;
	++m["msg"]; //如果没有该key，那么会获取到0.++会改变key的值。并把改变的key和v保存到map中。
	cout<<m.at("msg")<<endl;// at后面的key如果不存在，则崩溃！ 所以用[]访问key比较方便点。
	
	//添加k v的方式和修改存在的k的v。一样。如果该k不存在，则添加k v，如果k存在，则修改v的值为=后面的值
	m["key"]="value";// m是map<string,string>
	m["key"] = 333;	// m的类型是map<string,int>
	
	通过iterator访问map元素,first获取iterator指向元素的key，second获取value
	std::map<string,int> m;
	//m添加元素
	std::map<string,int>::iterator it(m.begin());
	while(it!=m.end())
	{
		cout<<"key:"<<it->first<<"\tvalue:"<<it->second<<endl;
		it++;
	}
	
	
10：继续第9的map内容。之前说过用at访问某个key的V的时候，可能该map不存在该key，则程序崩溃。如何避免
	at到不存在的Key呢？
	用find来测试是否存在该key
	std::map<string,int> m;
	//m添加元素...
	std::map<string,int>::iterator it = m.find("good");
	if(it == m.end())
		cout<<"not found..\n";
	else
		cout<<it->first<<it->second<<endl;
    	
	
11: 当vector类型变量被用const修饰的时候，需要用const_iterator 来代替iterator来迭代vector。

[code]
void printVector(std::vector<int>const & p)
{
    std::vector<int>::const_iterator it(p.begin());
    //....
}

	
12:  用transform来直接访问容器中的元素
    std::transform(_IIter,IIter,_Oiter,funName);
    _I的前缀表示输入。_O表示输出
    所以前两个参数是输入迭代器，用于表明范围，第三个是输出迭代器，
    由于transform会保证第一和第二个迭代器之间的每个元素都会调用一次funName;
    并且把每次funName这个方法的返回结果放在_Oiter所指向的位置。所以_Oiter一般和
    第一个参数相同。或指向后续有足够空间的另一个容器的位置@
    
    对funName的要求，返回类型与参数的类型和_IIter指向的类型相同!
	
    
    
14: 谓词是指那些返回值是bool的方法。谓词类方法使用在std::sort方法中。如下
    第五条中的std::sort(_IIter,_IIter);中缺少谓词，既排序的依据。
    默认是使用<来排序。即从小到达。我们可以自己定义排序规则。注意：
    pred(a,a) 返回false。
    谓词方法的参数，必须是_IIter所指向的类型。
    自定义的谓词方法：
    bool lowToUp(T const& t1, T const& t2)
    {
        if(t1<t2) return true;  //这里一定用<，不用<=
        else return false;
        
        //其实就一句 return t1<t2;
    }
    
    因为std::sort默认就是从小到大。所以
    std::sort(_vector.begin(), _vector.end())
    等同于
    std::sort(_vector.begin(), _vector.end(), lowToUP);

    

15: 注意如何使用string::iterator 来访问和操作string中的字符。
    
    //过滤掉string中某些类型的字符。
    bool cleanstring(string& str)
    {
        std::string::iterator it = str.begin();
        while(it!=str.end())
        {
            if(std::isdigit(*it))   //需要被过滤掉的字符。
            {
                cout<<"*it:"<<*it<<endl;
                str.erase(it,it+1);
                cout<<"*it:"<<*it<<endl;
                continue;   //erase后，it自动指向了erase第二个参数的位置。
            }
            it++;
        }
    }

    
  
   
16: 函数调用操作符，返回值任意，参数个数，类型任意。
    类的static成员，如果被const修饰，那么在声明的时候，就能赋予初始值。
    否则只能在类外定义！且注意static只是声明关键字，不能用于定义！(类外定义的时候，省去不能写static！);
   
   returnType operator()(...);
   
   [code]
   class createNum
   {
    public:
        int operator()(void)
        {
            return 3;
        }
   }
   
   void main()
   {
        createNum creater;
        cout<<creater()<<endl;  //会调用creater所声明的operator()() 方法！！！
   }
   
   
   
   page 274
17：常见算法:
    a>线性查找
        _OItyer std::find(_IIter,_IIter, key);


   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   

   
   
   
   
