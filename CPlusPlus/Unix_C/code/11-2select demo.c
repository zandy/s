//select demo
#include <sys/select.h>
#include <unistd.h>
#include <sys/time.h>

/*
	fds 维护宏函数。
	   void FD_CLR(int fd, fd_set *set);
       int  FD_ISSET(int fd, fd_set *set);
       void FD_SET(int fd, fd_set *set);
       void FD_ZERO(fd_set *set);

	   
	   int select(int nfds, fd_set *readfds, fd_set *writefds,
                  fd_set *exceptfds, struct timeval *timeout);
*/

int main()
{
	//如果监视readfds，如果一旦读缓冲区中有数据，那么select返回。
	//并且不影响缓冲区的数据。
	fd_set readfds;
	FD_ZERO(&readfds);
	FD_SET(0, &readfds);
	
	char buf[10] = {0};
	int r = 0;
	while(1)
	{
		//函数一个开始执行到这里，由于监视的0中没有数据，
		//所以一直阻塞等待数据，一旦有数据，发送信号。select返回
		select(1, &readfds, 0, 0, 0);
		printf("has some words\n");
		//读取数据 如果不读取数据，select一直返回！
		r = read(0, buf, sizeof(buf)-1); buf[r] = 0;printf("%s\n", buf);
		//如果
	}	
	
	return 0;
}