#include <assert.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

//为设置信号量的初始值 使用。 val
union semun {
     int              val;    /* Value for SETVAL */
     struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
     unsigned short  *array;  /* Array for GETALL, SETALL */
     struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                 (Linux-specific) */
 };

int main()
{

    int r = 0;
    int key = 0;
    key = ftok(".", 1);
    int semID = 0;
	//创建或获取信号量
    //semID = semget(key, 1, IPC_CREAT|IPC_EXCL|0666);
    semID = semget(key, 1,0);
    assert(semID !=-1);

	//初始化信号量的值
    union semun value;
    value.val = 3;
	//       int semctl(int semid, int semnum, int cmd, ...);
	//	     semnum 表示需要操作的信号量数组下标。
	r = semctl(semID,0, SETVAL, value.val);
    assert(r!=-1);


    struct sembuf op[1];
    op[0].sem_flg = 0;
    op[0].sem_op = -1;	//如果sem_op=0那么如果信号量>0阻塞，只有信号量为0，才返回。解除阻塞。
    op[0].sem_num = 0;
	
    while(1)
    {
	
        printf("before op\n");
        semop(semID, op, 1);//1 表示op这个元素的个数。如果op数组有两个元素。那么该参数是2
        printf("after op\n\n");

    }

}
