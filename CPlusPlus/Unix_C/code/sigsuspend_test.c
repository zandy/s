#include <stdio.h>
#include <unistd.h>
#include <signal.h>
//模拟耗时操作。
void job(int i)
{
    printf("---\tbegin:%d\n",i);
    sleep(2);
    printf("###\tend:%d\n\n",i);
}

//完成一次耗时操作之后，处理收到的信号。
void fun(int sig)
{
    printf("===\treceive:%d\n",sig);
    return;
}

void main()
{

    int num =0;
    //for sigprocmask. 设置在耗时操作的时候不接受中断。sigprocmask
    signal(SIGINT, fun);
    sigset_t sigus;
    sigemptyset(&sigus);
    sigaddset(&sigus, SIGINT);	//ctrl+c 中断信号就是 SIGINT
    

    //for sigpending 收集耗时操作的时候收到的中断信号。sigpending()
    sigset_t sig_pending;
    sigemptyset(&sig_pending);

    //for sigsuspend	程序挂起处理中断需要的中断集配置。
    sigset_t sigs;
    sigemptyset(&sigs);

	sigprocmask(SIG_BLOCK,&sigus,NULL);
    for(num=0; num<4; num++)
    {
        job(num);
        sigpending(&sig_pending);
        if(sigismember(&sig_pending, SIGINT))
        {
            //因为开始for循环之前，指定了不被SIGINT打断，所以这个for循环中只有sigsuspend这个代码执行的时候
			//还能被信号打断。在进入sigsuspend的时候，实行sigsuspend的信号屏蔽(sigs集合的信号不能打断程序挂起，被屏蔽掉)。
			//如果收集到的信号不在sigs集合内，那么会触中断，
			//执行注册了该中断的方法，(如果没有注册该信号的方法，那么信号由系统处理，导致程序退出)
			//然后退出sigsuspend方法。然后继续执行sigus中断屏蔽。
			//注意sigprocmask和 sigsuspend之间的信号屏蔽切换。
            sigsuspend(&sigs);
        }
        printf("--\tprepare for job\n");
    }

    sigprocmask(SIG_UNBLOCK,&sigus,NULL);
    sleep(1);
    printf("job over..\n");
}
