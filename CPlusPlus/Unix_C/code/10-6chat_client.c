//CHAT Client
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <curses.h>

WINDOW* winfo;
WINDOW* wmsg;

int fd;
int r;
struct sockaddr_in addr;

int initSock()
{
    fd = socket(AF_INET, SOCK_STREAM, 0);
    printf("create socket :%m\n");
    //bind socket
    addr.sin_family = AF_INET;
    addr.sin_port = htons(11111);
    inet_aton("192.168.198.128", &addr.sin_addr);

    r = bind(fd, (struct sockaddr*)&addr, sizeof(addr));
    //printf("bind socket:%m\n");
    if(r==-1)
    {
       close(fd);
       return -1;
    }
    return 0;
}

int destory()
{

}
int initUI()
{
    initscr();
    winfo = derwin(stdscr,(LINES-3), COLS,0,0);
    wmsg = derwin(stdscr, 3, COLS, LINES-3, 0);
    box(winfo,0,0);
    box(wmsg,0,0);
    refresh();
    wrefresh(winfo);
    wrefresh(wmsg);

    return 0;
}

main()
{
    //fd = initSock();
    //printf("init sock:%m\n");
    //if(fd==-1) exit(-1);

    initUI();
    if(fork())
    {
        //父进程
        //输入发送消息
    } else
    {
        //子进程
        //接收显示消息
        exit(0);
    }
    getchar();
    //destory();
}
