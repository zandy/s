#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <assert.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>

int main()
{
    //build socket
    int fd = 0;
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    assert(fd!=-1);
    printf("socket success\n");

    //init target add
    struct sockaddr_in addr_target={0};
    addr_target.sin_family = AF_INET;
    addr_target.sin_port   = htons(55555);
    inet_aton("192.168.198.128",&addr_target.sin_addr);

    //
    char buf[100];
    int r;
    while(1)
    {
        r = read(0,buf, sizeof(buf)-1);
        buf[r] = 0;
        sendto(fd, buf, sizeof(buf)-1,0,
               (struct sockaddr*)&addr_target,
               sizeof(addr_target));

        printf("sent:%s\n", buf);

        {
            printf("RECV:");
			//这里recv不需要指定recv的端口号。
            r = recv(fd,buf,sizeof(buf)-1,0);
            buf[r]=0;
            printf("%s\n", buf);
        }
    }

    close(fd);
    return 0;
}
