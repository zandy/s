//消息队列
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/types.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//构建消息体
struct msgbuf_t
{
    long type;
    char buf[32];
};

main()
{
    int key = 0;
    int msgid;
    // 创建key，为创建消息队列准备
    key = ftok(".", 255);

    //get消息队列
    msgid = msgget(key, 0);

    struct msgbuf_t msg;

    //recv msg
    while(1)
    {
        bzero(&msg, sizeof(msg));
        msg.type = 1;
        msgrcv(msgid, &msg, sizeof(msg.buf), 1,
                             0);
        printf("msg:%s\n", msg.buf);

    }
    //删除消息队列。
    //msgctl
}
