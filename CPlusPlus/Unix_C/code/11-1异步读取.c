//异步输入
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>

void handle(int s)
{
	char buf[200];
	int r = 0;
	r = read(0, buf, sizeof(buf)-1);
	buf[r] = 0;
	printf(":%s", buf);
}
main()
{
	//当标准输入的缓冲发生改变， 则发送信号SIGIO.
	fcntl(0, F_SETFL, O_ASYNC);
	//指定上面的信号发送到的目标进程。
	fcntl(0, F_SETOWN, getpid());
	//注册IO信号！！
	signal(SIGIO, handle);
	while(1);
	return;
}
