// 注意 搜索"bug"，已经解决了个小bug
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

//发送的协议
/*
 首先发送文件名
 然后发送文件内容。

 发送文件名和内容前，必须先发送一个数字，通知服务器需要接受的字节数。服务器需要用MSG_WAITALL阻止SOCK_STREAM的负面影响。
 如果发送的0.
 那么表示通知文件发送完成。
 */

int main()
{
    //服务器的socket 描述符
    int fdSock = -1;
    //发送的文件描述符
    int fdFile = -1;

    //发送的文件
    char fileName[] = "fileSnd.c";
    char fileContent[20]={0};

    int size = -1;
    int r = 0;


    //socket
    fdSock = socket(AF_INET, SOCK_STREAM, 0);
    assert(fdSock!=-1);
    printf("socket success..\n");

    //connect 到固定的主机
    struct sockaddr_in addrServer = {0};
    addrServer.sin_family = AF_INET;
    addrServer.sin_port = htons(55555);
    inet_aton("192.168.198.128", &addrServer.sin_addr);
    r = connect(fdSock, (struct sockaddr*)&addrServer, sizeof(addrServer));
    assert(r!=-1);
    printf("connect success..\n");

    //open the file
    //fdFile = open(fileName, O_RDONLY, 0);
    fdFile = open(fileName, O_RDONLY, 0);
    assert(fdFile!=-1);
    printf("file open success..\n");


    //send file name;
    size = strlen(fileName);

    r = send(fdSock, &size, sizeof(size), 0);
    assert(r!=-1);
	
	//这个bug导致发送不同步，调试了半个多小时。
	//在发送的时候，关于发送和接收同步的问题 要万分小心！！！！！！
    //r = send(fdSock, fileName, sizeof(fileName), 0);
    r = send(fdSock, fileName, size, 0);
    
    assert(r!=-1);

    //send content
    printf("begin send content...\n");
    sleep(2);
    while(1)
    {
        usleep(200000);
        size = read(fdFile, fileContent, sizeof(fileContent)-2);
		printf("size:%d\n", size);
        if(size>0)
        {

            r = send(fdSock, &size, sizeof(size),0);
            printf("send:%d\n", size);
            assert(r!=-1);
            send(fdSock, fileContent, size,0);
            printf("send:%s\n\n", fileContent);
            assert(r!=-1);
        }

        if(size==-1) break;

        if(size== 0) break;

    }
    //send a msg for file end.
    size = 0;
    send(fdSock, &size, sizeof(size), 0);

    printf("send a file:%s over..\n", fileName);
    close(fdFile);
    close(fdSock);
    return 0;
}

