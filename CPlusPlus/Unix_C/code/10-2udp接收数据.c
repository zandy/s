//#include <bits/socket.h>
#include <sys/socket.h>
#include <sys/ipc.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>

#include <netdb.h>
#include <stdio.h>
#include <assert.h>

int main(int argc, char** argv)
{
    printf("pid:%d\n",getpid());
    int fd;
    struct sockaddr_in ad;
    struct protoent* p_protoent = NULL;
    p_protoent = getprotobyname("udp");
    assert(p_protoent!=NULL);
    //printf("id:%d\n", p_protoent->p_proto);
    fd = socket(AF_INET, SOCK_DGRAM, p_protoent->p_proto);
    assert(fd!=-1);
    printf("socket success..\n");

    int r = 0;
    ad.sin_family = AF_INET;
    ad.sin_port = htons(55555);//'s'for short
    inet_aton("192.168.198.128",&ad.sin_addr);
    r = bind(fd,(struct sockaddr*)&ad, sizeof(ad));
    assert(r!=-1);
    printf("bind success..\n");

    socklen_t len = 0;
    char buf[101];
    struct sockaddr_in addr_sendfrom = {0};
    while(1)
    {
        len = sizeof(addr_sendfrom);
        int r = recvfrom(fd, buf, sizeof(buf)-1, 0,
                 (struct sockaddr* )&addr_sendfrom, &len );

        if(r>0)
        {
            buf[r]=0;
            printf("getmsg:%s from %s\n",
                   buf,
                   inet_ntoa(addr_sendfrom.sin_addr));
        }
        if(r==0)
        {
            printf("error r=0\n");
            break;
        }
        if(r<0)
        {
            printf("r<0..error %d\n", r);
            break;
        }
    }

    close(fd);

    return 0;
}
