#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/select.h>

main()
{
	int sfd; //服务器描述符
	
	int fdall[100];//客服端描述符
	int count;//客服端个数
	int r;//保存方法返回值
	
	struct sockaddr_in dr;//ip地址和端口
	fd_set fds = 0;//select监控的描述符集合
	int maxfd = 0;//记录最大文件描述符
	int i;
	
	char buf[1024] = {0};
	//建立socket并bind
	sfd=socket(AF_INET, SOCK_STREAM, 0);//USE TCP protocol
	printf("creat socket:%m\n");
	if(sfd == -1) exit(-1);
	
	dr.sin_family = AF_INET;
	dr.sin_port = htons(8866);
	inet_aton("192.168.198.128", &dr.sin_addr);
	r = bind(sfd, (struct sockaddr*)&dr, sizeof(dr));
	printf("bind socket:%m\n");
	if(r == -1) 
	{	
		close(sfd);
		exit(-1);

	}	
		
	count = 0;
	maxfd = 0;
	FD_ZERO(&fds);
	for(i=0; i<100; i++)
	{
		fdall[i] = -1;//无效描述符
	}
	
	while(1)
	{
		//构造监听描述符
		FD_ZERO(&fds);
		maxfd=-1;
		//加入服务器描述符
		FD_SET(sfd, &fds);
		maxfd=maxfd>sfd?maxfd:sfd;
		//加入客服端描述符
		for(i=0; i<count; i++)
		{
			if(fdall[i] != -1)
			{
				FD_SET(fdall[i], &fds);
				maxfd=maxfd>=fdall[i]?maxfd, fdall[i];
			}
			
		}
		
		//使用select监控fds
		r = select(maxfd+1, &fds,0,0,0);
		if(r==-1)
		{
			printf("select: 服务器崩溃;%m\n");
		}
		//fds发生改变
		//1:有客服连接。有新客服端会影响 sfd，服务器描述符
		if(FD_ISSET(sfd,&fds)
		{
			fdall[count] = accept(sfd, 0, 0);
			printf("connect a new clinet\n");
			count++;
		}
		//2:客服端发数据
		for(i=0; i<count; i++)
		{
			//获取改变的描述符集合
			if(FD_ISSET(fdall[i])&& fdall[i]!=-1)
			{
				//
				r = recv(fdall[i], buf, sizeof(buf)-1, 0);
				if(r==0)//有客服断开
				{
					printf("有客服断开\n");
					close(fdall[i]);
					fdall[i] = -1;
				}
				
				if(r==-1)
				{
					printf("网络异常\n");
					exit(-1);
				}
				//广播
				if(r>0)
				{
					buf[r] = 0;
					int j = 0;
					for(j=0; j<count; j++)
					{
						if(fdall[j]!=-1)
						send(fdall[j],buf, r, 0);
					}
				}
				
			}
		}
	}

	
	close(sfd);
}


