/*
 * 在main中，通过子主程创建两个子进程
 *分别求出各自范围内的质数。放入管道中
 *主进程读取管道中的数据。保存运算结果。
 */

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
isprime(int pNum)
{
    if(pNum<2)
        return 0;
    else if(pNum==2)
        return 1;
    else {
        int num = 2;
        while(num<pNum)
        {
            if(pNum%num==0)
                return 0;
            num++;
        }
        return 1;
    }
    return 0;//no useful...
}

//当收到两个子进程结束的信号后，结束主进程。因为主进程被while(1)困住！
void delMainProAfterChild(int s)
{
    static int count = 0;
    if(s == SIGCHLD)
    {


        printf("%d is over\n", getpid());
        count ++;
        int status;
		//回收子进程 的进程节点
        wait(&status);

        if(count == 2)
        {
            printf("main(%d) proc will over\n", getpid());
            exit(0);
        }
    }

}

int main()
{
    int id = 0;
    int num1 = 0;
    int num2 = 0;

    int fd[2]={0};
	//打开匿名管道
    pipe(fd);

	//绑定信号，当子进程执行exit(0)后，会发送信号。
    signal(SIGCHLD,delMainProAfterChild);
    while(1)
    {
        if(id==0)
        {
            num1 = 0;
            num2 = 5000;
        } else if(id==1)
        {
            num1 = 5001;
            num2 = 10000;
        }

        if(fork())
        {
            id++;
			//保证存在id=0,id=1,id=2 三个进程。
            if(id==2)
            {
                break;
            }
            continue;
        } else
        {
            //child process bunisess!!所有的子进程进入此大括号内
            close(fd[0]);
            while(1)
            {
                int _num =0;
                for(_num=num1;_num<num2; _num++)
                {
                    if(isprime(_num))
                    {
                        write(fd[1], &_num, sizeof(_num));
                    }
                }
                close(fd[1]);
                usleep(10000);
                exit(0);
            }
        }
    }

    //save the primes to a file.
    while(1)
    {
        int prime = 0;
        read(fd[0], &prime, sizeof(prime));
        printf("prime:%d\n",prime);
		//主进程save的质数。
    }


    return 0;

}
