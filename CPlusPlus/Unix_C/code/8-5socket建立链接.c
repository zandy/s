#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <linux/un.h>

int main()
{
    //AF_UNIX表示通过本地文件建立socket。
	//SOCK_DGRAM 报文发送模式，每次发送的数据有边界。
	int fd = socket(AF_UNIX, SOCK_DGRAM, 0); 
	assert(fd>0);

   printf("%d:%d\n", getpid(), fd);

	//需要头文件 linux/un.h
	struct sockaddr_un addr={0};
	//该属性和socket的第一个参数相同。
	addr.sun_family = AF_UNIX;
	memcpy(addr.sun_path, "my.sock", strlen("my.sock"));

	//建立socket绑定到本地的一个文件上。
	int r = bind(fd, (struct sockaddr*)&addr, sizeof(addr.sun_path));
	assert(r!=-1);

   //accept data
	char buf[256]={0};
	r = read(fd, buf, sizeof(buf)-1);
	printf("read %d char\n", r);
	buf[r] = '\0';
    r = 0;

	printf("buf:%s\n", buf);


   //delete the socket file.
	unlink("my.sock");
	
    close(fd);
   return 0;
}
