#include <stdio.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>

//建立TCP服务器端，等待客服端的连接。
int main()
{
    int r = -1;
    int fdserver = -1;
    int fdclient = -1;

    struct sockaddr_in addr_server={0};
    struct sockaddr_in addr_clinet={0};
    //socket
    fdserver = socket(AF_INET, SOCK_STREAM, 0);
    assert(fdserver!=-1);
    printf("server: socket success\n");

    addr_server.sin_family = AF_INET;
    addr_server.sin_port = htons(55555);
    inet_aton("192.168.198.128",&addr_server.sin_addr);

    r = bind(fdserver,(struct sockaddr*)&addr_server, sizeof(addr_server));
    assert(r!=-1);
    printf("server: bind success\n");
    listen(fdserver, 3);

    socklen_t len = sizeof(addr_server);
    while(1)
    {
        printf("accpt..");
        fdclient = accept(fdserver,(struct sockaddr*)&addr_clinet,&len);

        assert(fdclient!=-1);
        printf("from:%s:%d\n\n",
               inet_ntoa(addr_clinet.sin_addr),
               ntohs(addr_clinet.sin_port)
               );
    }

    close(fdserver);
    return 0;
}
