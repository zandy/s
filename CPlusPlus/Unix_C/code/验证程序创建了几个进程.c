#include <stdio.h>
#include <unistd.h>

int show()
{
   printf("pid:%d\n", getpid());
}

int main()
{
	int i=0;
	for(i=0; i<3; i++)
	{
		//当i=1创建进程的时候，主进程从frok()返回,开始下一句执行。子进程指向frok()开始执行，但是返回值是0.
		//当i=2 之前的子进程也会通过fork 创建出子进程。
	   if(fork())
		{
		  
		}
	}
	//包括第一个主进程，一共存在2的三次方个进程。
    show();
	return 0;
}

/*

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int show()
{
   printf("pid:%d\n", getpid());
}
int main()
{
	int i=0;
	int id = 0;

	while(1)
	{
	   id ++;
		if(id==2)
		{
		   printf("have %d +1 procs\n", id);
		}
	   if(fork())
		{
		
		} else
		{
			//......子进程的业务。
			//子进程最好结束在这个位置，不要再次让它回到执行fork，否则容易失控！！
		   exit(0);
		}
	}

	show();
   return 0;
}


*/