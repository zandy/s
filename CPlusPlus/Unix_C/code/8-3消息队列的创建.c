//消息队列
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/types.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//构建消息体
struct msgbuf_t
{
    long type;
    char buf[32];
};

main()
{
    int key = 0;
    int msgid;
    // 创建key，为创建消息队列准备
    key = ftok(".", 255);

    //创建消息队列
    msgid = msgget(key, IPC_CREAT|IPC_EXCL|0644);

    struct msgbuf_t msg;

    //构建消息发送消息
    int i =0;
    while(i<10)
    {
        bzero(&msg, sizeof(msg));
        msg.type = 1;
        sprintf(msg.buf, "msg type(1):%d\n", i);
        msgsnd(msgid, &msg, sizeof(msg.buf), 0);
        i++;
    }
    //删除消息队列。
    //msgctl
}
