#include <signal.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <stdio.h>
#include <assert.h>

int* p = 0;
void del(int s)
{
    if(s == SIGINT)
    {
        shmdt(p);
    }
}

int main()
{
    int key =0;
    int shmid = 0;

    signal(SIGINT,del);
    key = ftok(".",255);
    //assert(key>0);
    shmid = shmget(key, sizeof(int), 0);

    assert(shmid>0);

    p = shmat(shmid, 0,0);
    assert(p>0);
    while(1)
    {
        printf("%d--int:%d\n", getpid(), *p);
        usleep(400000);
    }

}
