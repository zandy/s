#include <netdb.h>
#include <stdio.h>

main()
{
   struct hostent* ent;
   sethostent(1);//1表示有状态连接，需要endhostent 来断开。
   while(1)
   {   
      ent = gethostent();
      if(ent==0) break;

      printf("name:%s\n", ent->h_name);
      printf("ip:%s\n", ent->h_addr);
      printf("ip:%hhu.%hhu.%hhu.%hhu\n",
         ent->h_addr[0],
         ent->h_addr[1],
         ent->h_addr[2],
         ent->h_addr[3]);
      printf(".........\n");
   }   
   endhostent();
}
