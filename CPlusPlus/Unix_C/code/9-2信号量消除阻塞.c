#include <assert.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

union semun {
     int              val;    /* Value for SETVAL */
     struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
     unsigned short  *array;  /* Array for GETALL, SETALL */
     struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                 (Linux-specific) */
 };

int main()
{
    int r = 0;
    int key = 0;
    key = ftok(".", 1);
    int semID = 0;
	//1 表示信号量数组的元素个数。
    //semID = semget(key, 1, IPC_CREAT|IPC_EXCL|0666);
    semID = semget(key, 1,0);
    assert(semID !=-1);
    /*
    union semun value;
    value.val = 3;
    r = semctl(semID,0, SETVAL, value.val);
    assert(r!=-1);
    */

    struct sembuf op[1];
    op[0].sem_flg = 0;
    op[0].sem_op = 1;
    op[0].sem_num = 0;
    while(1)
    {

        semop(semID, op, 1);
        printf("add value\n");
        sleep(1);

    }




}
