//10-5_2接受文件.c

#include <stdio.h>
#include <assert.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
	int r = -1; //监视函数执行返回值
	//1:建立socket
	int fdSock = -1;
	fdSock = socket(AF_INET, SOCK_STREAM,0);
	assert(fdSock!=-1);
	printf("socket success:%d\n", fdSock);
	
	//2:绑定网络地址和端口号
	struct sockaddr_in host = {0};
	host.sin_port = htons(55555);
	host.sin_family = AF_INET;
	inet_aton("192.168.198.128",&host.sin_addr);
	r = bind(fdSock, (struct sockaddr*)&host, sizeof(host));
	assert(r!=-1)
	printf("bind success..\n");
	
	//监听并获取连接
	listen(fdSock, 3);//并发存在的连接数为3
	
	int fdCline = -1;
	int fdFile = -1;
	fdCline = accept(fdSock, 0, 0);
	accept(adCline!=-1);
	printf("获取连接");
	
	//3.1获取文件名，新建文件
	int len = 0;//需要接受的字节数
	char buf[100] = {0};
	r = recv(fdCline, &len, sizeof(len), MSG_WAITALL);//MSG_WAITALL 非常重要
	assert(r!=-1&&len<sizeof(buf));
	r = recv(fdCline, buf, len, MSG_WAITALL);
	buf[len] = 0;
	assert(r!=-1);
	
	fdFile = open(buf, O_RDWR|O_CREAT|O_EXCL, 0640);
	assert(fdFile!=-1);	
	printf("create file ok..\n");
	
	//读取文件内容
	while(1)
	{	
		r = recv(fdCline, &len, sizeof(len), MSG_WAITALL);
		if(len == 0) break;
		assert(r!=-1&&len<sizeof(buf);
		r = recv(fdCline, buf, len, MSG_WAITALL);
		assert(r!=-1);
	}
	//显示接受状态
	printf("recv over....!!!!\n");
	
	close(fdFile);
	close(fdSock);
	return 0;
}

