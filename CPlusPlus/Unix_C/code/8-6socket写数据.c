#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <linux/un.h>

int main()
{
   int fd = socket(AF_UNIX, SOCK_DGRAM, 0); 
	assert(fd>0);

   printf("%d:%d\n", getpid(), fd);

	struct sockaddr_un addr={0};
	addr.sun_family = AF_UNIX;
	memcpy(addr.sun_path, "my.sock", strlen("my.sock"));

   // bind error!! 
   //不能重复绑定，而是用connect来连接到一个存在的socket。
	//int r = bind(fd, (struct sockaddr*)&addr, sizeof(addr.sun_path));
	int r = connect(fd, (struct sockaddr*)&addr, sizeof(addr.sun_path));
	
	assert(r!=-1);

   /*
   //accept data
	char buf[256]={0};
	read(fd, buf, sizeof(buf)-1);
	assert(r>0);
	buf[r] = '\0';
	*/

   //write data
   //使用sendto 可以用connect指定目标。
   write(fd, "good game!!\n", sizeof("good game!!\n"));	
   close(fd);
   //delete the socket file.

   return 0;
}
