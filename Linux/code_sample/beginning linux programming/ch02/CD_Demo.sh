#	这个项目是 一个CD唱片的管理程序。用bash实现
#	数据保留在两个普通文件中。 一个文件保存一张cd的基本信息
#		目录编号cdcatnum,  标题 cdtitle 、曲目类型 cdtype、 作家 cdauthor
#	另一个文件已cdcatnum 最主键，列出其包含的 曲目编号, 曲名


#全局变量

menu_choice=""
current_cd=""
title_file="title.cdb"
tracks_file="tracks.cdb"
temp_file="/tmp/cdb.$$"

#退出时删除临时文件
trap 'rm -f $temp_file' EXIT

#工具函数
#获取用户输入
get_return()
{
	echo -e "press return \c"
	read x
	return 0
}

get_confirm()
{
	echo -e "Are you sure?\c"
	
	while true
	do
		read x
		case $x in
			y|yes|Y|Yes|YES)
			return 0
			;;
			n|no|N|No|NO)
			return 1
			;;
			*)
				echo "please enter yes or no"
			;;
		esac
	done
}




