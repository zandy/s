
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdio.h>
#include <poll.h>

int main()
{
    struct pollfd fds[2]={0};

    int ret;

    // watch stdin for input
    fds[0].fd = STDIN_FILENO;
    fds[0].events = POLLIN;

    //fds[1].fd = STDOUT_FILENO;
    //fds[1].fd = open("ttt", O_RDWR|O_CREAT,0644);
    //fds[1].events = POLLOUT;

    while(1)
    {
        //ret = poll(fds, 2, 1000);
        ret = poll(fds, 1, 1000*2);
        if(ret == -1) printf("%m");
        if(!ret)
        {
            printf("no evet\n");
        }
        if(fds[0].revents & POLLIN)
        {
            printf("+++read abele\n");
            char buf[100];
            int r=0;
            r = read(fds[0].fd, buf, sizeof(buf)-1);
            buf[r]=0;
            printf("write:%s\n", buf);
            fds[0].revents = 0;
        }
        if(fds[1].revents & POLLOUT)
        {
            printf("******write able\n");
            char buf[100];
            int r=0;
            r = write(fds[1].fd, buf, sizeof(buf)-1);
            buf[r]=0;
            printf("write:%s\n", buf);
            fds[1].revents = 0;
        }

    }
    return 0;

}

