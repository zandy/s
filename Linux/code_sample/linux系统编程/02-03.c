#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

int usr_interrupt=0;

void synch_signal(int sig)
{
	usr_interrupt=1;
}

void child_function()
{
	printf("child process, Myid:%d\n", getpid());
	sleep(3);
	kill(getppid(), SIGUSER1);
	puts(":) bye..\n");
	exit(0);
}

int main()
{
	struct sigaction usr_action;
	sigset_t block_mask;
	pid_t child_id;
	
	sigfillset(&block_mask);
	
	usr_action.sa_handler = synch_signal;
	usr_action.sa_mask = block_mask;
	usr_action.sa_flags = 0;
	sigaction(SIGUSER1, &user_action, NULL);
	child_id = fork();
	if(child_id==0)
	 child_function();
	 while(!user_interrupt);
	 puts("over..\n");
	return 0;
}