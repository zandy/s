

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <linux/stat.h>
#include <fcntl.h>

#define FIFO_FILE "MYFIFO"

int main(int arg, char** argc)
{
        int fd = open(FIFO_FILE, O_WRONLY);
		  printf("fd:%d\n", fd);
		  char msg[] = "gdd..";
		  if(arg == 2)
		  {
		     write(fd, argc[1], strlen(argc[1]));
			  return 0;
		  }
		  write(fd, msg, sizeof(msg));
		  
        return 0;
}
