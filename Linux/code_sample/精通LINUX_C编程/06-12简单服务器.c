/*service*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <linux/stat.h>

#define FIFO_FILE "MYFIFO"

int main()
{
	FILE* fp;
	char readbuf[1024];
	if((fp = fopen(FIFO_FILE,"r") == NULL)
	{
		umask(0);
		mknod(FIFO_FILE, S_IFIOF|0666,0);
	} else
	{
		fclose(fp);
	}
	
	
		if((fp =fopen(FIFO_FILE, "R")==NULL) 
		{
			printf("%m\n");
			return 1;
		}
		
	while(1)
	{
		if(fgets(readbuf,80, fp)!==NULL)
		{
			printf("recv:%s\n", readbuf);
		}
	}
	fclose(fp);
	return 0;
}
