/*service*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <linux/stat.h>
#include <fcntl.h>

#define FIFO_FILE "MYFIFO"

int main()
{
		  /*
        FILE* fp;
        char readbuf[1024] = {0};
        if((fp = fopen(FIFO_FILE,"r")) == NULL)
        {
					 printf("creat file\n");
                umask(0);
                mknod(FIFO_FILE, S_IFIFO|0666,0);
        } 
		  else
        {
		        printf("has file\n");
                fclose(fp);
        }

		  printf("pare file:fifo.\n");
        if((fp =fopen(FIFO_FILE, "r"))==NULL)
        {
								printf("error..\n");
                        return 1;
        } 

		  printf("open file success\n");
        while(1)
        {
                if(fgets(readbuf,80, fp)!=NULL)
                {
                        printf("recv:%s\n", readbuf);
                }
        }
        fclose(fp);
		  */
		  //mkfifo("MYFIFO", 0644);
		  int fd = open("MYFIFO", O_RDONLY);
		  if(fd==-1) return 1;
		  printf("fd:%d\n", fd);
		  char msg[1024]={0};
		  int r = 0;
		  while((r = read(fd,msg, sizeof(msg)-1)) != -1 )
		  {
		      //r = read(fd, msg, sizeof(msg)-1);
				if(r == 0) continue;
				msg[r] = 0;
				printf("r :%d recv:%s\n", r, msg);
		  }
		  close(fd);
        return 0;
}
