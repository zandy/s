#!/bin/bash

echo ${name:-ssm}	#因为name还没有定义，或为空.所以显示ssm。name仍然为空
echo ${name}		#显示空

echo ${name:=ssm}	#因为name为空，或没有定义.所以name初始化为 ssm
echo "after:"
echo $name			#显示 ssm

echo ${name:=bbs}	#因为 name已经有值。所以显示其旧值
echo "after:"
echo $name			#显示ssm

echo "over"
