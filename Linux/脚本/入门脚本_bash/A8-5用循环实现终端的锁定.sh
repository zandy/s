#!/bin/bash

trap '' 1 2 3 18 #忽略了的信号
stty -echo	#stty不回显

echo -n "key:"
read key1
echo

echo -n "again:"
read key2
echo 

if [ $key1 = $key2 ] 
   then
      tput clear  #清屏
      until [ "$key3" = "$key1" ]
      do
         echo
         echo -n "password:"
         read key3
      done
fi
