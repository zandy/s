/*
  *在主程序中使用widget 作为主窗口。
  *添加控件的方法：
     创建QHBoxLayout or QVBoxLayout.
     然后添加控件到layout中。
     为主窗口设置layout
  */
#include <QApplication>
#include <QSpinBox>
#include <QSlider>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    //创建窗口
    QWidget* window = new QWidget();
    window->setWindowTitle("Enter you name:");

    //设置尺寸
    window->setFixedSize(300,60);

    QSpinBox* spinBox = new QSpinBox;
    QSlider* slider = new QSlider(Qt::Horizontal);

    //设置取值范围
    spinBox->setRange(0,130);
    slider->setRange(0,130);

    QObject::connect(spinBox, SIGNAL(valueChanged(int)),
                     slider, SLOT(setValue(int))
                     );

    QObject::connect(slider, SIGNAL(valueChanged(int)),
                     spinBox, SLOT(setValue(int))
                     );

    QHBoxLayout* layout = new QHBoxLayout;
    // QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(spinBox);
    layout->addWidget(slider);
    window->setLayout(layout);
    window->show();
    return app.exec();
}
