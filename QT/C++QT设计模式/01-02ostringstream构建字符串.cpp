/*使用ostringstream 构建字符串 sprintf 同理可以实现*/
#include <QCoreApplication>
#include <sstream>
#include <iostream>
#include <fstream>
using namespace std;
int main(int argc, char *argv[])
{
	//Qt的命令行模式。 和 a.exec 呼应
    QCoreApplication a(argc, argv);
    ostringstream buf;
    char sex='F';
    int age=25;
    string name="SSM";
    //构造字符串
    buf<<"name:"<<name<<std::endl;
    buf<<"age:"<<age<<endl;
    buf<<"sex:"<<sex<<endl;
    string str = buf.str();
    qDebug(str.c_str());
    ofstream of("./data");
    if(of.is_open())
    {
        of<<str<<endl;
    }

    of.close();
     cout<<of<<endl;
    //输出到文件中
    return a.exec();
}
