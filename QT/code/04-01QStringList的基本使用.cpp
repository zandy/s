#include <QStringList>
#include <QDebug>

/*
  *使用QDebug 输出控制台信息。
  *使用QStringList 添加QString成员。
  */
int main(int argc, char *argv[])
{
    QString winter = "Decomber, Jan, Feb";
    QString spring = "Mar, Apri, MAy";
    QString sum = "June, july, August.";
    QString fail = "Sep,Oct, Nove";

    //添加QString成员到QList中。通过 append , <<, +=
    QStringList list;
    list<<winter;
    list += spring;
    list.append(sum);
    list<<fail;

    //打印某个成员
    qDebug()<<"The spri:"<<list.at(1);

    //元素1，元素2，元素3 ...最后一个元素  无分隔符号结尾。
    QString months = list.join(",");
    qDebug()<<months<<endl;

    //join的逆运算。
    QStringList list2 = months.split(",");
    qDebug()<<"list2:\n"<<list2<<endl;

    //使用标准cpp 的iterator 来遍历元素。
    QStringList::Iterator it(list2.begin());
    while(it!=list2.end())
    {

        qDebug()<<"*it:"<<*it<<endl;
         it++;
    }

    //使用foreach 遍历元素
    foreach(const QString& str, list2)
    {
        qDebug()<<"foreach:"<<str<<endl;
    }
    //java风格遍历元素
    QListIterator<QString> itr(list2);
    while(itr.hasNext())
    {
        QString cur = itr.next();

    }

    qDebug()<<"Over\n";
    return 0;
}
