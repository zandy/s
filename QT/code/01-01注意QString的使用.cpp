#include <QString>
#include <QTextStream>
//自定义cin cout
QTextStream cout(stdout);
QTextStream cin(stdin);

int main()
{
    QString str1("Good");
    QString str2("game");
    cout<<str1<<endl;
    str1 += str2;
    cout<<str1<<endl;
    cout<<"str2:"<<endl;

	//读取一行数据。
    str2 = cin.readLine();
    cout<<str2<<endl;
    return 0;
}
