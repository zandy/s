#include <QApplication>
#include <QSpinBox>
#include <QSlider>
#include <QWidget>
#include <QHBoxLayout>
int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    QWidget manWin;

    //初始化两个控件，设置其取值返回，并绑定相关的信号槽
    QSpinBox spinbox;
    QSlider     slider(Qt::Horizontal);
    spinbox.setRange(0,100);
    slider.setRange(0,100);
    QObject::connect(&spinbox, SIGNAL(valueChanged(int)), &slider, SLOT(setValue(int)));
    QObject::connect(&slider, SIGNAL(valueChanged(int)), &spinbox, SLOT(setValue(int)));

    //将控件添加到布局中。并设置QWidget实例的布局。
    QHBoxLayout layout;
    manWin.setWindowTitle("title");
    layout.addWidget(&spinbox);
    layout.addWidget(&slider);
    manWin.setLayout(&layout);
    manWin.show();
    return app.exec();
}
